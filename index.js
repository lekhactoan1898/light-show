const puppeteer = require("puppeteer");

(async () => {
  const productId = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  //await page.setViewport({width: 800,height: 800})

  while (1) {
    let index = Math.floor(Math.random() * 1000) % productId.length;
    await page.goto(`https://panama.vn/products/${productId[index]}`);
    await page.waitFor(10000);
    await page.evaluate(scrollToBottom);
    await page.waitFor(1000);
  }
})();

async function scrollToBottom() {
  await new Promise(resolve => {
    const distance = 40;
    const delay = 100;
    const timer = setInterval(() => {
      document.scrollingElement.scrollBy(0, distance);
      if (
        document.scrollingElement.scrollTop + window.innerHeight >=
        document.scrollingElement.scrollHeight - 800
      ) {
        clearInterval(timer);
        resolve();
      }
    }, delay);
  });
}
